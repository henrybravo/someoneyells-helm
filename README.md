# someoneyells-helm

example helm chart with the someoneyells demo application with Amazon ECR as container image source

**_not for public use - to deploy you need to have permissions to pull from this ecr repository_**

**_you can still use this chart by switching to the public branch of this repository_**

## how to be used

get the chart

```
$ git clone https://gitlab.com/henrybravo/someoneyells-helm.git
```

deploy the chart

```
$ helm install --name someoneyells-helm ./someoneyells-helm
```

test deployment

```
$ kubectl get svc -w <SERVICE NAME> -o wide
```

package chart for sharing

```
$ helm package someoneyells-helm
```

## delete deployment

list the deployed charts

```
$ helm list

NAME                REVISION    UPDATED                     STATUS      CHART                   NAMESPACE
someoneyells-helm	1       	Wed Sep  5 16:00:06 2018	DEPLOYED	someoneyells-helm-0.1.0	default
```

Delete the deployed chart

```
$ helm delete someoneyells-helm
```